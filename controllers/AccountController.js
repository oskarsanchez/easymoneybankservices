const requestJSON = require('request-json');
const mlabBaseURL = "https://api.mlab.com/api/1/databases/easymoneybank/collections/accounts/";
const mlabAPIKey = "apiKey=" + process.env.MLAB_API_KEY;
const util = require('../utils/util');
const logger = util.logger;
const BANK_CODE = "0977";
const BRANCH_CODE = "6666";
const COUNTRY_CODE = "ES";

// get accounts of a user
function getAccountsV1 (req,res){
  logger.info("GET /easymoney/v1/users/:user_id/accounts");
  logger.debug("El id del usuario:" + req.params.user_id);
  let userId = req.params.user_id;
  // Check if product exist
  if (userId == undefined){
    res.status(500);
    responseJSON = {"msg": "Error obteniendo cuentas"};
  }

  let query = 'q={"user_id":' + userId + '}';
  let mlabUrl = "?" + query +"&" +  mlabAPIKey;
  logger.debug("mlabUrl" + mlabUrl);
  let responseJSON = {};
  let httpClient = requestJSON.createClient(mlabBaseURL);
  httpClient.get(mlabUrl,
    function(err, resMLab, body) {
      if (err){
          res.status(500);
          responseJSON = {"msg": "Error obteniendo cuentas"};
      } else {
        logger.debug("body");
        logger.debug(body);
        if (body.length > 0) {
          responseJSON = body;
        } else{
          responseJSON = {"msg": "El usuario no tiene cuentas"};
          res.status(404);
        }
      }
      res.send(responseJSON);
    }
  )
}

// get account
function getAccountV1 (req,res){
  logger.info("GET /easymoney/v1/accounts/:account_id");
  logger.debug("El id de la cuenta:" + req.params.account_id);

  let accountId = req.params.account_id;
  let query = 'q={"id":' + accountId + '}';
  let mlabUrl = "?" + query +"&" +  mlabAPIKey;
  logger.debug("mlabUrl" + mlabUrl);

  let responseJSON = {};
  let httpClient = requestJSON.createClient(mlabBaseURL);
  httpClient.get(mlabUrl,
    function(err, resMLab, body) {
      logger.debug(res.statusCode);
      if (err){
          res.status(500);
          responseJSON = {"msg": "Error obteniendo la cuenta"};
          responseJSON.id = accountId;
      } else {
        if (body.length > 0) {
          responseJSON = body;
        } else{
          res.status(404);
          responseJSON.id = accountId;
          responseJSON = {"msg": "Cuenta no encontrada"};

        }
      }
      res.send(responseJSON);
    }
  );
}

function createAccountV1 (req,res){
  logger.info("POST /easymoney/v1/users/:user_id/accounts");
  logger.debug("El id del usuario:" + req.params.user_id);
  logger.debug("JSON POST");
  logger.debug(req.body);
  let newAccount= {};
  let responseJSON = {};
  if (!_checkAccountField(req)){
    logger.info("_checkAccountField false");
    responseJSON = {"msg": "Error insertando cuenta"};
    res.status(400);
    res.send(responseJSON);
  }
  else{
    let mlabUrl = '?f={"id":1}&s={"id":-1}&l=1&' + mlabAPIKey;
    logger.debug("mlabUrl" + mlabUrl);
    let httpClient = requestJSON.createClient(mlabBaseURL);
    httpClient.get(mlabUrl,
      function(err, resMLab, body) {
        logger.debug("get last id");
        if (err){
            logger.info("Eror 500 al consultar el ultimo id");
            res.status(500);
            responseJSON = {"msg": "Error al consultar el ultimo id"};
            res.send(responseJSON);
        } else {
            let id = body[0].id + 1;
            newAccount = {
                "id": id, // mandatory
                "user_id": parseInt(req.params.user_id), // mandatory
                "iban": util.createIBAN(COUNTRY_CODE,BANK_CODE,BRANCH_CODE),
                "balance": 0,
                "currency": req.body.currency, // mandatory
                "currency_des": (req.body.currency == "EUR")? "€" : "$", // mandatory
                "conditions": req.body.conditions, //mandatory
                "type": req.body.type //mandatory
              }
              logger.debug("newAccount:");
              logger.debug(newAccount);
              let mlabUrl = "?" + mlabAPIKey;
              logger.debug("mlabUrl" + mlabUrl);
              let httpClient = requestJSON.createClient(mlabBaseURL);
              httpClient.post(mlabUrl, newAccount,
                function(err, resMLab, body) {
                  logger.debug(resMLab.statusCode);
                  logger.debug("Cuenta guardada con exito");
                  if (err){
                    res.status(400); //bad request
                    responseJSON = {"msg": "Error creando cuenta"};
                  }else {
                    res.status(201); //created
                    responseJSON = {"msg": "Cuenta creada correctamente"};
                  }
                  res.send(responseJSON);
                }
              );
            }
        });
  }
}

function updateAccountV1 (req,res){
  logger.info("PUT /easymoney/v1/accounts/:account_id updateAccountV1");
}

function updatePartialAccountV1 (req,res){
  logger.info("PATCH /easymoney/v1/accounts/:account_id updatePartialAccountV1");

}

function deleteAccountV1 (req,res){
  logger.info("DELETE /easymoney/v1/accounts/:account_id deleteAccountV1");
}

function _checkAccountField(req){
  logger.debug("_checkAccountField");
  if (req.params.user_id == undefined || req.params.user_id == "0"
      || req.body.currency == undefined || req.body.currency == ""
      || req.body.conditions == undefined || req.body.conditions.length == 0
      || req.body.type == undefined || req.body.type == "")
    return false;
  return true;
}

module.exports.getAccountsV1 = getAccountsV1;
module.exports.getAccountV1 = getAccountV1;
module.exports.createAccountV1 = createAccountV1;
module.exports.updateAccountV1 = updateAccountV1;
module.exports.updatePartialAccountV1 = updatePartialAccountV1;
module.exports.deleteAccountV1 = deleteAccountV1;
