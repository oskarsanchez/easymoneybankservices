const requestJSON = require('request-json');
const util = require('../utils/util');
const mlabAccountURL = "https://api.mlab.com/api/1/databases/easymoneybank/collections/accounts/"
const mlabMovementsURL = "https://api.mlab.com/api/1/databases/easymoneybank/collections/movements/";
const mlabAPIKey = "apiKey=" + process.env.MLAB_API_KEY;
const logger = util.logger;



function createTransferV1 (req,res){
  logger.info("POST /easymoney/v1/accounts/:account_id/transfers");
  logger.debug(req.params.account_id);
  logger.debug(req.body);
  let newTransfer = {};
  let responseJSON = {};

  if (!checkTransferField(req)){
    logger.info("checkTransferField false");
    responseJSON = {"msg": "Error creando transferencia"};
    res.status(400);
    res.send(responseJSON);
  }
  else{
    let mlabUrl = '?f={"id":1}&s={"id":-1}&l=1&' + mlabAPIKey;
    logger.debug("mlabUrl" + mlabUrl);
    let httpClient = requestJSON.createClient(mlabMovementsURL);
    httpClient.get(mlabUrl,
      function(err, resMLab, body) {
        logger.debug("get last id");
        if (err){
            logger.info("Eror 500 al consultar el ultimo id");
            res.status(500);
            responseJSON = {"msg": "Error al consultar el ultimo id"};
            res.send(responseJSON);
        } else {
            let id = 1;
            if (body[0] != null)
              id =  parseInt(body[0].id) + 1;
            newMovement = {
              "id": id,
              "type": req.body.type,
              "amount": req.body.amount * -1,
              "currency": req.body.currency,
              "currency_des": (req.body.currency == "EUR")? "€" : "$", //mandatory
              "category" : req.body.category,
              "subcategory" : req.body.type + " Realizada",
              "date" : req.body.date,
              "source" : req.body.source,
              "subject" : req.body.subject,
              "beneficiary": req.body.beneficiary,
              "iban_target": req.body.iban_target,
              "account_target_id":  req.body.account_target_id,
              // "detail" :  req.body.detail,
              // "address" : req.body.address,
              // "city" : req.body.city,
              // "community" : req.body.community,
              // "country" : req.body.country,
            //  "location_x" : req.body.location_x,
            //  "location_y" : req.body.location_y,
              "account_id": parseInt(req.params.account_id)
            //  "client_id" : "client_id"
            }
            logger.debug("newMovement:");
            logger.debug(newMovement);
            let mlabUrl = "?" + mlabAPIKey;
            logger.debug("mlabUrl" + mlabUrl);
            let httpClient = requestJSON.createClient(mlabMovementsURL);
            httpClient.post(mlabUrl, newMovement,
              function(err, resMLab, body) {
                logger.debug("Movimiento guardado con exito");
                if (err){
                  res.status(400); //bad request
                  responseJSON = {"msg": "Error insertando movimiento"};
                  res.send(responseJSON);
                }else {
                  let mlabUrl = '?f={"balance":1}&q={"id":'+ newMovement.account_id +'}&' + mlabAPIKey;
                  logger.debug("mlabUrl" + mlabUrl);
                  let httpClient = requestJSON.createClient(mlabAccountURL);
                  httpClient.get(mlabUrl,
                    function(err, resMLab, body) {
                      if (err){
                        res.status(400); //bad request
                        responseJSON = {"msg": "Error insertando movimiento"};
                        res.send(responseJSON);
                      }else {
                        let balance = body[0].balance;
                        logger.debug("balance:" + balance);
                        balance += newMovement.amount;
                        logger.debug("new balance:" + balance);
                        let mlabUrl = '?q={"id":'+ newMovement.account_id +'}&' + mlabAPIKey;
                        logger.debug("mlabUrl" + mlabUrl);
                        let putBody = '{"$set":{"balance":'+balance+'}}';
                        let httpClient = requestJSON.createClient(mlabAccountURL);
                        httpClient.put(mlabUrl, JSON.parse(putBody),
                          function(err, resMLab, body) {
                            if (err){
                              res.status(400); //bad request
                              responseJSON = {"msg": "Error insertando movimiento"};
                              res.send(responseJSON);
                            }else {
                              if (req.body.type == "Transferencia"){
                                res.status(201);
                                responseJSON = {"msg": "Transferencia realizada correctamente"};
                                responseJSON.accountBalance = balance;
                                logger.debug("response:" + res.statusCode);
                                logger.debug(responseJSON);
                                res.send(responseJSON);
                              }
                              else{ // Traspaso

                                let mlabUrl = '?f={"id":1}&s={"id":-1}&l=1&' + mlabAPIKey;
                                logger.debug("mlabUrl" + mlabUrl);
                                let httpClient = requestJSON.createClient(mlabMovementsURL);
                                httpClient.get(mlabUrl,
                                  function(err, resMLab, body) {
                                    logger.debug("get last id");
                                    if (err){
                                        logger.info("Eror 500 al consultar el ultimo id");
                                        res.status(500);
                                        responseJSON = {"msg": "Error al consultar el ultimo id"};
                                        res.send(responseJSON);
                                    } else {
                                        let id = 1;
                                        if (body[0] != null)
                                          id =  parseInt(body[0].id) + 1;

                                        newMovement = {
                                          "id": id,
                                          "type": req.body.type,
                                          "amount": req.body.amount,
                                          "currency": req.body.currency,
                                          "currency_des": (req.body.currency == "EUR")? "€" : "$", //mandatory
                                          "category" : req.body.category,
                                          "subcategory" : req.body.type + " Recibido",
                                          "date" : req.body.date,
                                          "source" : req.body.source,
                                          "subject" : req.body.subject,
                                          //"iban_target": req.body.iban_target,
                                          //"account_target_id": req.body.account_target_id,
                                          // "detail" :  req.body.detail,
                                          // "address" : req.body.address,
                                          // "city" : req.body.city,
                                          // "community" : req.body.community,
                                          // "country" : req.body.country,
                                        //  "location_x" : req.body.location_x,
                                        //  "location_y" : req.body.location_y,
                                          "account_id": req.body.account_target_id
                                        //  "client_id" : "client_id"
                                        }
                                        logger.debug("newMovement:");
                                        logger.debug(newMovement);
                                        let mlabUrl = "?" + mlabAPIKey;
                                        logger.debug("mlabUrl" + mlabUrl);
                                        let httpClient = requestJSON.createClient(mlabMovementsURL);
                                        httpClient.post(mlabUrl, newMovement,
                                          function(err, resMLab, body) {
                                            logger.debug("Movimiento guardado con exito");
                                            if (err){
                                              res.status(400); //bad request
                                              responseJSON = {"msg": "Error insertando movimiento"};
                                              res.send(responseJSON);
                                            }else {
                                              let mlabUrl = '?f={"balance":1}&q={"id":'+ newMovement.account_id +'}&' + mlabAPIKey;
                                              logger.debug("mlabUrl" + mlabUrl);
                                              let httpClient = requestJSON.createClient(mlabAccountURL);
                                              httpClient.get(mlabUrl,
                                                function(err, resMLab, body) {
                                                  if (err){
                                                    res.status(400); //bad request
                                                    responseJSON = {"msg": "Error insertando movimiento"};
                                                    res.send(responseJSON);
                                                  }else {
                                                    let balanceTarget = body[0].balance;
                                                    logger.debug("balance:" + balanceTarget);
                                                    balanceTarget += newMovement.amount;
                                                    logger.debug("new balance:" + balanceTarget);
                                                    let mlabUrl = '?q={"id":'+ newMovement.account_id +'}&' + mlabAPIKey;
                                                    logger.debug("mlabUrl" + mlabUrl);
                                                    let putBody = '{"$set":{"balance":'+balanceTarget+'}}';
                                                    let httpClient = requestJSON.createClient(mlabAccountURL);
                                                    httpClient.put(mlabUrl, JSON.parse(putBody),
                                                      function(err, resMLab, body) {
                                                        if (err){
                                                          res.status(400); //bad request
                                                          responseJSON = {"msg": "Error insertando movimiento"};
                                                          res.send(responseJSON);
                                                        }else {
                                                            res.status(201);
                                                            responseJSON = {"msg": "Traspaso realizado correctamente"};
                                                            responseJSON.accountBalance = balance;
                                                            logger.debug("response:" + res.statusCode);
                                                            logger.debug(responseJSON);
                                                            res.send(responseJSON);
                                                        }
                                                      });
                                                    }
                                                  });
                                                }
                                              });
                                            }
                                          });
                              }
                            }
                          });
                        }
                      });
                    }
                  });
            }
          });
        }
}



function checkTransferField(req){
  logger.debug("checkTransferField");

  if (req.body.type == "Traspaso" &&
     (req.body.account_target_id == undefined || req.body.account_target_id == "0" ))
    return false;

  if (req.body.type == "Transferencia" && (req.body.iban_target == undefined || req.body.iban_target == "" )
      && (req.body.beneficiary == undefined || req.body.beneficiary == ""))
      return false;

  if (req.body.type == undefined || req.body.type == ""
      || req.body.amount == undefined || req.body.amount == ""
      || req.body.date == undefined || req.body.date == ""
      || req.body.subject == undefined || req.body.subject == ""
      || req.params.account_id == undefined || req.params.account_id == "0"
      || req.body.currency == undefined || req.body.currency == "" )
    return false;
  return true;
}

module.exports.createTransferV1 = createTransferV1;
