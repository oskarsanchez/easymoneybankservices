const requestJSON = require('request-json');
const util = require('../utils/util');
const mlabAccountURL = "https://api.mlab.com/api/1/databases/easymoneybank/collections/accounts/"
const mlabBaseURL = "https://api.mlab.com/api/1/databases/easymoneybank/collections/movements/";
const mlabAPIKey = "apiKey=" + process.env.MLAB_API_KEY;
const fixerBaseURL = "http://data.fixer.io/api/";
const fixerAPIKey  ="access_key=" + process.env.FIXER_API_KEY
const logger = util.logger;

// get a movement
function getMovementV1 (req,res){
  logger.info("GET /easymoney/v1/movements/:movement_id");
  logger.debug("El id del movimiento:" + req.params.movement_id);

  let movementId = req.params.movement_id;
  let query = 'q={"id":' + movementId + '}';
  let mlabUrl = "?" + query +"&" +  mlabAPIKey;
  logger.debug("mlabUrl" + mlabUrl);

  let responseJSON = {};
  let httpClient = requestJSON.createClient(mlabBaseURL);
  httpClient.get(mlabUrl,
    function(err, resMLab, body) {
      logger.debug(res.statusCode);
      if (err){
          res.status(500);
          responseJSON = {"msg": "Error obteniendo movimiento"};
          responseJSON.id = movementId;
      } else {
        if (body.length > 0) {
          responseJSON = body;
        } else{
          res.status(404);
          responseJSON.id = movementId;
          responseJSON = {"msg": "Movimiento no encontrado"};

        }
      }
      res.send(responseJSON);

    }
  );
}

function getMovementsV1 (req,res){
  logger.info("GET /easymoney/v1/accounts/:account_id/movements");
  logger.debug("El id del producto:" + req.params.account_id);
  let accountId = req.params.account_id;
  // Check if product exist
  if (accountId == undefined){
    res.status(500);
    responseJSON = {"msg": "Error obteniendo movimientos"};
  }

  let query = 'q={"account_id":' + accountId + '}';
  let mlabUrl = "?" + query +"&" +  mlabAPIKey;
  logger.debug("mlabUrl" + mlabUrl);
  let responseJSON = {};
  let httpClient = requestJSON.createClient(mlabBaseURL);
  httpClient.get(mlabUrl,
    function(err, resMLab, body) {
      if (err){
          res.status(500);
          responseJSON = {"msg": "Error obteniendo movimientos"};
      } else {
        if (body.length > 0) {
          responseJSON = body;
        } else{
          responseJSON = {"msg": "Producto no encontrado"};
          res.status(404);
        }
      }
      res.send(responseJSON);
    }
  )
}

function createMovementV1 (req,res){
  logger.info("POST /easymoney/v1/accounts/:account_id/movements");
  logger.debug(req.params.account_id);
  logger.debug(req.body);
  let newMovement = {};
  let responseJSON = {};
  if (!checkMovementField(req)){
    logger.info("checkMovementField false");
    responseJSON = {"msg": "Error insertando movimiento"};
    res.status(400);
    res.send(responseJSON);
  }
  else{
    logger.debug("Currency:" + req.body.currency);
    if (req.body.currency != "EUR"){
      logger.debug("Currency != EUR");

      let fixerUrl = 'latest?'+ fixerAPIKey + '&symbols=' + req.body.currency;
      logger.debug("fixerUrl" + fixerUrl);
      let httpClient = requestJSON.createClient(fixerBaseURL);
      httpClient.get(fixerUrl,
        function(err, resFixer, bodyFixer) {
          logger.debug("get last currency");
          if (err){
              logger.info("Eror 500 al obtener el cambio de divisa");
              res.status(500);
              responseJSON = {"msg": "Error al obtner el cambio de divisa"};
              res.send(responseJSON);
          } else {
              let rate = 1;
              if (bodyFixer != null)
                rate = bodyFixer.rates[req.body.currency];
              logger.debug("rate:" + rate);
              let mlabUrl = '?f={"id":1}&s={"id":-1}&l=1&' + mlabAPIKey;
              logger.debug("mlabUrl" + mlabUrl);
              let httpClient = requestJSON.createClient(mlabBaseURL);
              httpClient.get(mlabUrl,
                function(err, resMLab, body) {
                  logger.debug("get last id");
                  if (err){
                      logger.info("Eror 500 al consultar el ultimo id");
                      res.status(500);
                      responseJSON = {"msg": "Error al consultar el ultimo id"};
                      res.send(responseJSON);
                  } else {
                      let id = 1;
                      if (body[0] != null)
                        id =  parseInt(body[0].id) + 1;
                      newMovement = {
                        "id": id,
                        "type": req.body.type,
                        "amount": (req.body.type == "Ingreso") ? req.body.amount * rate : req.body.amount * rate * -1,
                        "amount_converted": req.body.amount,
                        "currency": "EUR",
                        "currency_des": "€",
                        "currency_converted": req.body.currency,
                        "currency_des_converted": (req.body.currency == "EUR")? "€" : "$", //mandatory //mandatory
                        "category" : req.body.category,
                        "subcategory" : req.body.type,
                        "date" : req.body.date,
                        "source" : req.body.source,
                        "subject" : req.body.subject,
                        "detail" :  req.body.detail,
                        "address" : req.body.address,
                        "city" : req.body.city,
                        "community" : req.body.community,
                        "country" : req.body.country,
                        "account_id": parseInt(req.params.account_id)
                      }

                      logger.debug("newMovement:");
                      logger.debug(newMovement);
                      let mlabUrl = "?" + mlabAPIKey;
                      logger.debug("mlabUrl" + mlabUrl);
                      let httpClient = requestJSON.createClient(mlabBaseURL);
                      httpClient.post(mlabUrl, newMovement,
                        function(err, resMLab, body) {
                          logger.debug("Movimiento guardado con exito");
                          if (err){
                            res.status(400); //bad request
                            responseJSON = {"msg": "Error insertando movimiento"};
                            res.send(responseJSON);
                          }else {
                            let mlabUrl = '?f={"balance":1}&q={"id":'+ newMovement.account_id +'}&' + mlabAPIKey;
                            logger.debug("mlabUrl" + mlabUrl);
                            let httpClient = requestJSON.createClient(mlabAccountURL);
                            httpClient.get(mlabUrl,
                              function(err, resMLab, body) {
                                if (err){
                                  res.status(400); //bad request
                                  responseJSON = {"msg": "Error insertando movimiento"};
                                  res.send(responseJSON);
                                }else {
                                  let balance = body[0].balance;
                                  logger.debug("balance:" + balance);
                                  balance += newMovement.amount;
                                  logger.debug("new balance:" + balance);
                                  let mlabUrl = '?q={"id":'+ newMovement.account_id +'}&' + mlabAPIKey;
                                  logger.debug("mlabUrl" + mlabUrl);
                                  let putBody = '{"$set":{"balance":'+balance+'}}';
                                  let httpClient = requestJSON.createClient(mlabAccountURL);
                                  httpClient.put(mlabUrl, JSON.parse(putBody),
                                    function(err, resMLab, body) {
                                      if (err){
                                        res.status(400); //bad request
                                        responseJSON = {"msg": "Error insertando movimiento"};
                                        res.send(responseJSON);
                                      }else {
                                        res.status(201);
                                        responseJSON = {"msg": "Movimiento insertando correctamente"};
                                        responseJSON.accountBalance = balance;
                                        logger.debug("response:" + res.statusCode);
                                        logger.debug(responseJSON);
                                        res.send(responseJSON);
                                      }
                                    });
                                  }
                                });
                              }
                            });
                      }
                    });
                  }
        });
    }
    else{
      logger.debug("Currency = EUR");
      let mlabUrl = '?f={"id":1}&s={"id":-1}&l=1&' + mlabAPIKey;
      logger.debug("mlabUrl" + mlabUrl);
      let httpClient = requestJSON.createClient(mlabBaseURL);
      httpClient.get(mlabUrl,
        function(err, resMLab, body) {
          logger.debug("get last id");
          if (err){
              logger.info("Eror 500 al consultar el ultimo id");
              res.status(500);
              responseJSON = {"msg": "Error al consultar el ultimo id"};
              res.send(responseJSON);
          } else {
              let id = 1;
              if (body[0] != null)
                id =  parseInt(body[0].id) + 1;
              newMovement = {
                "id": id,
                "type": req.body.type,
                "amount": (req.body.type == "Ingreso")? req.body.amount: req.body.amount * -1,
                "currency": req.body.currency,
                "currency_des": (req.body.currency == "EUR")? "€" : "$", //mandatory
                "category" : req.body.category,
                "subcategory" : req.body.type,
                "date" : req.body.date,
                "source" : req.body.source,
                "subject" : req.body.subject,
                "detail" :  req.body.detail,
                "address" : req.body.address,
                "city" : req.body.city,
                "community" : req.body.community,
                "country" : req.body.country,
                "account_id": parseInt(req.params.account_id)
              }
              logger.debug("newMovement:");
              logger.debug(newMovement);
              let mlabUrl = "?" + mlabAPIKey;
              logger.debug("mlabUrl" + mlabUrl);
              let httpClient = requestJSON.createClient(mlabBaseURL);
              httpClient.post(mlabUrl, newMovement,
                function(err, resMLab, body) {
                  logger.debug("Movimiento guardado con exito");
                  if (err){
                    res.status(400); //bad request
                    responseJSON = {"msg": "Error insertando movimiento"};
                    res.send(responseJSON);
                  }else {
                    let mlabUrl = '?f={"balance":1}&q={"id":'+ newMovement.account_id +'}&' + mlabAPIKey;
                    logger.debug("mlabUrl" + mlabUrl);
                    let httpClient = requestJSON.createClient(mlabAccountURL);
                    httpClient.get(mlabUrl,
                      function(err, resMLab, body) {
                        if (err){
                          res.status(400); //bad request
                          responseJSON = {"msg": "Error insertando movimiento"};
                          res.send(responseJSON);
                        }else {
                          let balance = body[0].balance;
                          logger.debug("balance:" + balance);
                          balance += newMovement.amount;
                          logger.debug("new balance:" + balance);
                          let mlabUrl = '?q={"id":'+ newMovement.account_id +'}&' + mlabAPIKey;
                          logger.debug("mlabUrl" + mlabUrl);
                          let putBody = '{"$set":{"balance":'+balance+'}}';
                          let httpClient = requestJSON.createClient(mlabAccountURL);
                          httpClient.put(mlabUrl, JSON.parse(putBody),
                            function(err, resMLab, body) {
                              if (err){
                                res.status(400); //bad request
                                responseJSON = {"msg": "Error insertando movimiento"};
                                res.send(responseJSON);
                              }else {
                                res.status(201);
                                responseJSON = {"msg": "Movimiento insertando correctamente"};
                                responseJSON.accountBalance = balance;
                                logger.debug("response:" + res.statusCode);
                                logger.debug(responseJSON);
                                res.send(responseJSON);
                              }
                            });
                          }
                        });
                      }
                    });
              }
            });
          }
        }
}

function updateMovementV1 (req,res){
  logger.info("PUT /apitechu/v2/users updateMovementV1");
}

function updatePartialMovementV1 (req,res){
  logger.info("PATCH /apitechu/v2/users updateMovementV1");

}

function deleteMovementV1 (req,res){
  logger.info("DELETE /apitechu/v2/users deleteMovementV1");
}

function checkMovementField(req){
  logger.debug("checkMovementField");
  if (req.body.type == undefined || req.body.type == ""
      || req.body.amount == undefined || req.body.amount == ""
      || req.body.date == undefined || req.body.date == ""
      || req.body.subject == undefined || req.body.subject == ""
      || req.params.account_id == undefined || req.params.account_id == "0"
      || req.body.currency == undefined || req.body.currency == "" )
    return false;
  return true;
}

module.exports.getMovementsV1 = getMovementsV1;
module.exports.getMovementV1 = getMovementV1;
module.exports.createMovementV1 = createMovementV1
module.exports.updateMovementV1 = updateMovementV1;
module.exports.updatePartialMovementV1 = updatePartialMovementV1;
module.exports.deleteMovementV1 = deleteMovementV1;
