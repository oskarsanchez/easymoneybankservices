const ibantools = require('ibantools');
const winston = require('winston');

const logger = winston.createLogger({
  //level: 'info',
  format: winston.format.json(),
  transports: [
    //
    // - Write to all logs with level `info` and below to `combined.log`
    // - Write all logs error (and below) to `error.log`.
    //
    new winston.transports.File({ filename: 'easyMoneyBankServices_error.log', level: 'error' }),
    new winston.transports.File({ filename: 'easyMoneyBankServices.log', level: 'debug'})
  ]
});

//
// If we're not in production then log to the `console` with the format:
// `${info.level}: ${info.message} JSON.stringify({ ...rest }) `
//
if (process.env.ENV != 'live') {
  logger.add(new winston.transports.Console({
    format: winston.format.simple()
  }));
}

function createIBAN(country, bank, branch){
  logger.debug("createIBAN, country:" + country + ", bank: " + bank + ", branch:" + branch);
  let params = {};
  params.bban =  bank + branch + _getRandomInt(10,99) + _getRandomInt(1000000001,9999999999);
  params.countryCode = country;
  let iban = ibantools.composeIBAN(params);
  logger.debug("BBAN:" + params.bban);
  logger.debug("iban: " + iban);
  return iban;
}
function validateIBAN(iban){
   return ibantools.composeIBAN("ES", "ABNA0417164300");
}

function _getRandomInt(min, max) {
  return Math.floor(Math.random() * (max - min)) + min;
}

module.exports.logger = logger;
module.exports.createIBAN = createIBAN;
