
const jwt = require('jsonwebtoken');

function createToken (userId) {
  console.log("create Token user ID: " + userId);
  let payload = {
                  data: userId
                }
  let token = jwt.sign(payload, process.env.SECRET, { expiresIn: '1h' });
  console.log("token:" + token);
  return token;
}

function isValidToken (decodeToken, userId) {
  console.log("isValidToken");
  return (decodeToken.data == userId) ? true : false;
}

function verifyToken (token, userId) {
  console.log("verifyToken");
  let decodedToken;
  try {
    decodedToken = jwt.verify(token, process.env.SECRET);
  } catch(err) {
    return false;
  }
  return (!isValidToken(decodedToken, userId)) ? false : true;
}

function isAuthenticated (req, res, next) {
  console.log("isAuthenticated");
  if(!req.headers.authorization) {
    res.status(403);
    let responseJSON = {"msg" : "Tu petición no tiene cabecera de autorización"}
    return res.send(responseJSON);
  }
  let token = req.headers.authorization.split(" ")[1];
  let decodedToken;

  try {
    decodedToken = jwt.verify(token, process.env.SECRET);
  } catch(err) {
    res.status(401);
    let responseJSON = (err.name == 'TokenExpiredError') ?
      {"msg" : "El token ha expirado"} : {"msg" : "Token invalido"};
    console.log(responseJSON);
    return res.send(responseJSON);
  }

  if (!isValidToken(decodedToken, parseInt(req.headers.user_id))) {
    res.status(401);
    let responseJSON = {"msg" : "No tiene permisos para realizar la operación"}
    return res.send(responseJSON);
  }

  next();

}

module.exports.createToken = createToken;
module.exports.isAuthenticated = isAuthenticated;
module.exports.verifyToken = verifyToken;
