require('dotenv').config();

const express = require('express');
const app = express();
const port = process.env.PORT || 3000;

const movementController = require('./controllers/MovementController');
const accountController = require('./controllers/AccountController');
const transferController = require('./controllers/TransferController');

app.use(express.json());

var enableCORS = function(req, res, next) {
 // No producciserviceón!!!11!!!11one!!1!
 res.set("Access-Control-Allow-Origin", "*");
 res.set("Access-Control-Allow-Methods", "POST, GET, OPTIONS, DELETE, PUT");
 res.set("Access-Control-Allow-Headers","*");
 // res.set("Access-Control-Allow-Headers","Content-Type");
 next();
}
app.use(enableCORS);

const authJWT = require('./utils/auth');

// Movements routes
app.get("/easymoney/v1/accounts/:account_id/movements", authJWT.isAuthenticated, movementController.getMovementsV1);
app.get("/easymoney/v1/movements/:movement_id",authJWT.isAuthenticated, movementController.getMovementV1);
app.post("/easymoney/v1/accounts/:account_id/movements", authJWT.isAuthenticated, movementController.createMovementV1);
app.put("/easymoney/v1/movements/:movement_id", authJWT.isAuthenticated, movementController.updateMovementV1);
app.patch("/easymoney/v1/movements/:movement_id",authJWT.isAuthenticated, movementController.updatePartialMovementV1);
app.delete("/easymoney/v1/movements/:movement_id", authJWT.isAuthenticated, movementController.deleteMovementV1);

// Accounts routes
app.get("/easymoney/v1/users/:user_id/accounts", authJWT.isAuthenticated, accountController.getAccountsV1);
app.get("/easymoney/v1/accounts/:account_id", authJWT.isAuthenticated, accountController.getAccountV1);
app.post("/easymoney/v1/users/:user_id/accounts", authJWT.isAuthenticated, accountController.createAccountV1);
app.put("/easymoney/v1/accounts/:account_id", authJWT.isAuthenticated, accountController.updateAccountV1);
app.patch("/easymoney/v1/accounts/:account_id", authJWT.isAuthenticated, accountController.updatePartialAccountV1);
app.delete("/easymoney/v1/accounts/:account_id",authJWT.isAuthenticated, accountController.deleteAccountV1);

// Transfers routes
app.post("/easymoney/v1/accounts/:account_id/transfers", authJWT.isAuthenticated, transferController.createTransferV1);

app.listen(port,
  function(){
    console.log ("Easy Money Bank - Bank Services is running !!! In Port:" + port);
  }
);
