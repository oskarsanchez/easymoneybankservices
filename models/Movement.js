

const movement = {
  "id": number, //mandatory
  "amount":number,//mandatory
  "currency": string, //mandatory
  "currency_des": string, //mandatory
  "type": string, //mandatory
  "category" : string,
  "subcategory" : string,
  "date" : string, //mandatory
  "source" : string,
  "subject" : string, //mandatory
  "detail" :  string,
  "address" : string,
  "city" : string,
  "community" : string,
  "country" : string,
  //"location_x" : string,
  //"location_y" : string,
  "account_id": number, //mandatory
//  "client_id" : number
}

const movementExample = {
  "id": 1,
  "type": "tarjeta|transferencia|recibo|ingreso|reintegro",
  "amount": 124.78,
  "currency":"EUR",
  "currency_des":"€",
  "category" : "ingreso|gasto",
  "subcategory" : "colegio",
  "date" : "28/10/2018",
  "source" : "H Bilingual School Torrejon de Ardoz SL",
  "subject" : "Adeudo nº 281880808087637467673",
  "detail" :  "Extraescolar Gimnasia rítmiza",
  "address" : "Calle Mariano Benlliure 1",
  "city" : "Torrejon de Ardoz",
  "community" : "Madrid"
  "country" : "España",
//  "location_x" : string,
//  "location_y" : string,
  "account_id": 1,
  //"client_id" : 1
}
module.exports.movement = Movement;
