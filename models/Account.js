const accountExample = {
  "id":1, // mandatory
  "user_id":6, // mandatory
  "iban":"NL89 VSXV 5058 1691 57", //mandatory
  "balance":40317.51,
  "currency":"EUR", // mandatory
  "currency_des":"€", 
  "conditions":"Mauris sit amet eros. Suspendisse accumsan tortor quis turpis. Sed ante. Vivamus tortor. Duis mattis egestas metus.", //mandatory
  "type":"Cuenta de negocio" //mandatory
};

const account = {
  "id": Number, // mandatory
  "user_id": Number, // mandatory
  "iban": String, //mandatory
  "balance": Number,
  "currency": String, // mandatory
  "currency_des": String, // mandatory
  "conditions": String, //mandatory
  "type": String //mandatory
};

module.exports.account = account;
