const mocha = require ('mocha');
const chai = require ('chai');
const chaihttp = require('chai-http');
const requestJSON = require('request-json');
const auth = require("../utils/auth");

chai.use(chaihttp);

const should = chai.should();

const mlabBaseURL = "https://api.mlab.com/api/1/databases/easymoneybank/collections/";
const mlabAPIKey = "apiKey=" + process.env.MLAB_API_KEY;

const URL_ACCOUNT_SERVICES = process.env.URL_BANK_SERVICES || "http://localhost:3010/easymoney/v1/";

const ACCOUNT_TEST = 1;
const USER_TEST = 99

let token;


///easymoney/v1/accounts/990005/movements
describe ('Test de API de Accounts Services',
  function(){
    before(function() {

      const server = require('../server');
      token = auth.createToken (USER_TEST);

    });

    after(function() {
      // runs after all tests in this block

    });

    beforeEach(function() {
      // runs before each test in this block
    });
    afterEach(function() {
      // runs after each test in this block
    });
    it('Test create a account of a user', function(done){
      let dataJSON = {
        "user_id":USER_TEST, // mandatory
        "currency":"EUR", // mandatory
        "conditions":[
          {
            "title": "CONDICIONES",
            "items": [{
                "key": "Periodicidad de liquidación",
                "isAmount": false,
                "value": "SEMESTRAL"
              },
              {
                "key": "Tipo de Interés Acreedor",
                "isAmount": true,
                "value": {
                  "amount": 0,
                  "currency": "EUR"
                }
              },
              {
                  "key": "Comisión de Administración",
                  "isAmount": true,
                  "value": {
                    "amount": 0,
                    "currency": "EUR"
                  }
            }]
          }
        ], //mandatory
        "type":"Cuenta personal"
      }
      chai.request(URL_ACCOUNT_SERVICES)
        .post('users/' + USER_TEST + '/accounts')
        .set('authorization', 'JWT ' + token)
        .set('user_id', USER_TEST)
        .send(dataJSON)
        .end(
          function(err,res){
            console.log("Testing "+ 'users/' + USER_TEST + '/accounts');
            console.log(err);
            should.not.exist(err);
            should.exist(res);
            res.should.have.status(201);
            res.body.should.have.property("msg");
            done();
          }
        )
      }
    ),
    it('Test create a account of a user that not exist', function(done){
      let dataJSON = {
          "user_id": 0, // mandatory
          "currency":"EUR", // mandatory
          "conditions":[
            {
              "title": "CONDICIONES",
              "items": [{
                  "key": "Periodicidad de liquidación",
                  "isAmount": false,
                  "value": "SEMESTRAL"
                },
                {
                  "key": "Tipo de Interés Acreedor",
                  "isAmount": true,
                  "value": {
                    "amount": 0,
                    "currency": "EUR"
                  }
                },
                {
                    "key": "Comisión de Administración",
                    "isAmount": true,
                    "value": {
                      "amount": 0,
                      "currency": "EUR"
                    }
              }]
            }
          ], //mandatory
          "type":"Cuenta personal"
      }
      chai.request(URL_ACCOUNT_SERVICES)
        .post('users/0/accounts')
        .set('authorization', 'JWT ' + token)
        .set('user_id', USER_TEST)
        .send(dataJSON)
        .end(
          function(err,res){
            console.log("Testing POST users/0/accounts");
            console.log(err);
            should.not.exist(err);
            should.exist(res);
            res.should.have.status(400);
            res.body.should.have.property("msg");
            done();
          }
        )
      }
    ),
    it('Test create a account of a user, that account has not mandatory property', function(done){
      let dataJSON = {
          "user_id": USER_TEST, // mandatory
          //"currency":"EUR", // mandatory
          "conditions":[
            {
              "title": "CONDICIONES",
              "items": [{
                  "key": "Periodicidad de liquidación",
                  "isAmount": false,
                  "value": "SEMESTRAL"
                },
                {
                  "key": "Tipo de Interés Acreedor",
                  "isAmount": true,
                  "value": {
                    "amount": 0,
                    "currency": "EUR"
                  }
                },
                {
                    "key": "Comisión de Administración",
                    "isAmount": true,
                    "value": {
                      "amount": 0,
                      "currency": "EUR"
                    }
              }]
            }
          ], //mandatory
          "type":"Cuenta personal"
      }
      chai.request(URL_ACCOUNT_SERVICES)
        .post('users/' + USER_TEST + '/accounts')
        .set('authorization', 'JWT ' + token)
        .set('user_id', USER_TEST)
        .send(dataJSON)
        .end(
          function(err,res){
            console.log("Testing POST " + 'users/' + USER_TEST + '/accounts');
            console.log(err);
            should.not.exist(err);
            should.exist(res);
            res.should.have.status(400);
            res.body.should.have.property("msg");
            done();
          }
        )
      }
    ),
    it('Test get accounts of a user', function(done){
      chai.request(URL_ACCOUNT_SERVICES)
        .get('users/' + USER_TEST + '/accounts')
        .set('authorization', 'JWT ' + token)
        .set('user_id', USER_TEST)
        .end(
          function(err,res){
            console.log("Testing " + 'users/' + USER_TEST + '/accounts');
            console.log(err);
            should.not.exist(err);
            should.exist(res);
            res.should.have.status(200);
            res.body.should.be.an('array');
            res.body.should.not.have.lengthOf(0);
            for (account of res.body){
              account.should.have.property("id");
              account.should.have.property("iban");
              account.should.have.property("type");
              account.should.have.property("user_id");
              account.should.have.property("currency");
              account.should.have.property("balance");
              account.should.have.property("conditions");
            }
            done();
          }
        )
      }
    ),
    it('Test get accounts of a user that no exist', function(done){
      chai.request(URL_ACCOUNT_SERVICES)
        .get('users/0/accounts')
        .set('authorization', 'JWT ' + token)
        .set('user_id', USER_TEST)
        .end(
          function(err,res){
            console.log("Testing users/0/accounts");
            console.log(err);
            should.not.exist(err);
            should.exist(res);
            res.should.have.status(404);
            res.body.should.have.property("msg");
            done();
          }
        )
      }
    ),
    it('Test get a account', function(done){
      chai.request(URL_ACCOUNT_SERVICES)
        .get('accounts/' + ACCOUNT_TEST)
        .set('authorization', 'JWT ' + token)
        .set('user_id', USER_TEST)
        .end(
          function(err,res){
            console.log('accounts/' + ACCOUNT_TEST);
            console.log(err);
            should.not.exist(err);
            should.exist(res);
            res.should.have.status(200);
            res.body.should.be.an('array');
            res.body.should.have.lengthOf(1);
            for (account of res.body){
              account.should.have.property("id");
              account.should.have.property("iban");
              account.should.have.property("type");
              account.should.have.property("user_id");
              account.should.have.property("currency");
              account.should.have.property("balance");
              account.should.have.property("conditions");
            }
            done();
          }
        )
      }
    ),
    it('Test get a account  that no exist', function(done){
      chai.request(URL_ACCOUNT_SERVICES)
        .get('accounts/0')
        .set('authorization', 'JWT ' + token)
        .set('user_id', USER_TEST)
        .end(
          function(err,res){
            console.log("Testing accounts/0");
            console.log(err);
            should.not.exist(err);
            should.exist(res);
            res.should.have.status(404);
            res.body.should.have.property("msg");
            done();
          }
        )
      }
    )

  }
);

function deleteCollecion(collection){
  console.log("deleteCollecion... collection:" + collection );
  let mlabUrl = collection + "?" + mlabAPIKey;
  console.log("mlabUrl" + mlabUrl);
  let httpClient = requestJSON.createClient(mlabBaseURL);
  httpClient.put(mlabUrl, [],
    function(err, resMLab, body) {
      console.log("Response mLab:" + resMLab.statusCode);
      return (!err) ? true : false;
    }
  );
}

function insertManyCollection(collection,file){
  console.log("insertManyCollection... collection:" + collection + ", file:" + file);
  let movements = require(file);
  let mlabUrl = collection + "?" + mlabAPIKey;
  console.log("mlabUrl" + mlabUrl);
  let responseJSON = {};
  let httpClient = requestJSON.createClient(mlabBaseURL);
  httpClient.post(mlabUrl, movements,
    function(err, resMLab, body) {
      console.log("Response mLab:" + resMLab.statusCode);
      return (!err) ? true : false;
    }
  );
}
