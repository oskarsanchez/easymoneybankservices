const mocha = require ('mocha');
const chai = require ('chai');
const chaihttp = require('chai-http');
const requestJSON = require('request-json');
const auth = require("../utils/auth");

chai.use(chaihttp);
const should = chai.should();

const mlabBaseURL = "https://api.mlab.com/api/1/databases/easymoneybank/collections/";
const mlabAPIKey = "apiKey=" + process.env.MLAB_API_KEY;
const URL_MOVEMENT_SERVICES = process.env.URL_BANK_SERVICES || "http://localhost:3010/easymoney/v1/";
const ACCOUNT_TEST = 22;
const MOVEMENT_TEST = 1;
const USER_TEST = 99

let token;

/*function sleep(ms){
   return new Promise(resolve=>{
       setTimeout(resolve,ms)
   });
}*/

///easymoney/v1/accounts/990005/movements
describe ('Test de API de Movement Services',
  function(){
    before(function() {
      // runs before all tests in this block
      const server = require('../server');
      token = auth.createToken (USER_TEST);
      //  deleteCollecion("movements");
      //  insertManyCollection("movements", "./movements.json");
      // ver cuando investigue promesas
      //  await sleep(1000);

    });

    after(function() {
      // runs after all tests in this block

    });

    beforeEach(function() {
      // runs before each test in this block
    });
    afterEach(function() {
      // runs after each test in this block
    });

    it('Test get movements of a account', function(done){
      chai.request(URL_MOVEMENT_SERVICES)
        .get('accounts/' + ACCOUNT_TEST + '/movements')
        .set('authorization', 'JWT ' + token)
        .set('user_id', USER_TEST)
        .end(
          function(err,res){
            console.log("Testing " + 'accounts/' + ACCOUNT_TEST + '/movements');
            console.log(err);
            should.not.exist(err);
            should.exist(res);
            res.should.have.status(200);
            res.body.should.be.an('array');
            res.body.should.not.have.lengthOf(0);
            for (movement of res.body){
              movement.should.have.property("id");
              movement.should.have.property("amount");
              movement.should.have.property("type");
              movement.should.have.property("date");
              movement.should.have.property("subject");
              movement.should.have.property("account_id");
            }
            done();
          }
        )
      }
    ),
    it('Test get movements of a account that no exist', function(done){
      chai.request(URL_MOVEMENT_SERVICES)
        .get('accounts/0/movements')
        .set('authorization', 'JWT ' + token)
        .set('user_id', USER_TEST)
        .end(
          function(err,res){
            console.log("Testing accounts/0/movements");
            console.log(err);
            should.not.exist(err);
            should.exist(res);
            res.should.have.status(404);
            res.body.should.have.property("msg");
            done();
          }
        )
      }
    ),
    it('Test get a movement', function(done){
      chai.request(URL_MOVEMENT_SERVICES)
        .get('movements/1')
        .set('authorization', 'JWT ' + token)
        .set('user_id', USER_TEST)
        .end(
          function(err,res){
            console.log("movements/1");
            console.log(err);
            should.not.exist(err);
            should.exist(res);
            res.should.have.status(200);
            res.body.should.be.an('array');
            res.body.should.have.lengthOf(1);
            for (movement of res.body){
              movement.should.have.property("id");
              movement.should.have.property("amount");
              movement.should.have.property("type");
              movement.should.have.property("date");
              movement.should.have.property("subject");
              movement.should.have.property("account_id");
            }
            done();
          }
        )
      }
    ),
    it('Test get a movement that no exist', function(done){
      chai.request(URL_MOVEMENT_SERVICES)
        .get('movements/0')
        .set('authorization', 'JWT ' + token)
        .set('user_id', USER_TEST)
        .end(
          function(err,res){
            console.log("Testing movements/0");
            console.log(err);
            should.not.exist(err);
            should.exist(res);
            res.should.have.status(404);
            res.body.should.have.property("msg");
            done();
          }
        )
      }
    ),
    it('Test create a movement of a account', function(done){
      let dataJSON = {
	       "type": "Ingreso",
	       "amount": 538.16,
         "currency": "EUR",
	       "category": "Ingreso",
	       "subcategory": "Alimentación",
	       "date": "24/03/2018",
	       "source": "Photolist",
	       "subject": "Pears - Bosc",
	       "detail": "nisi vulputate nonummy maecenas tincidunt lacus at velit vivamus vel nulla eget eros elementum pellentesque quisque porta volutpat erat",
	       "address": "3 Bultman Park",
	       "city": "Cuenca",
	       "community": "Andalucia",
	       "country": "España",
	       "account_id": ACCOUNT_TEST
      }
      chai.request(URL_MOVEMENT_SERVICES)
        .post('accounts/' + ACCOUNT_TEST + '/movements')
        .set('authorization', 'JWT ' + token)
        .set('user_id', USER_TEST)
        .send(dataJSON)
        .end(
          function(err,res){
            console.log("Testing "+ "accounts/" + ACCOUNT_TEST + "/movements");
            console.log(err);
            should.not.exist(err);
            should.exist(res);
            res.should.have.status(201);
            res.body.should.have.property("msg");
            done();
          }
        )
      }
    ),
    it('Test create a movement of a account that not exist', function(done){
      let dataJSON = {
         "type": "Ingreso",
         "amount": 538.16,
         "currency": "EUR",
         "category": "Ingreso",
         "subcategory": "Alimentación",
         "date": "24/03/2018",
         "source": "Photolist",
         "subject": "Pears - Bosc",
         "detail": "nisi vulputate nonummy maecenas tincidunt lacus at velit vivamus vel nulla eget eros elementum pellentesque quisque porta volutpat erat",
         "address": "3 Bultman Park",
         "city": "Cuenca",
         "community": "Andalucia",
         "country": "Sweden",
         "account_id": 0
      }
      chai.request(URL_MOVEMENT_SERVICES)
        .post('accounts/0/movements')
        .set('authorization', 'JWT ' + token)
        .set('user_id', USER_TEST)
        .send(dataJSON)
        .end(
          function(err,res){
            console.log("Testing POST accounts/0/movements");
            console.log(err);
            should.not.exist(err);
            should.exist(res);
            res.should.have.status(400);
            res.body.should.have.property("msg");
            done();
          }
        )
      }
    ),
    it('Test create a movement of a account that has not madatory property', function(done){
      let dataJSON = {
         "type": "Ingreso",
         "amount": 538.16,
         "date": "24/03/2018",
         "source": "Photolist",
         "subject": "Pears - Bosc",
         "account_id": ACCOUNT_TEST
      }
      chai.request(URL_MOVEMENT_SERVICES)
        .post('accounts/' + ACCOUNT_TEST + '/movements')
        .set('authorization', 'JWT ' + token)
        .set('user_id', USER_TEST)
        .send(dataJSON)
        .end(
          function(err,res){
            console.log("Testing POST " + 'accounts/' + ACCOUNT_TEST + '/movements');
            console.log(err);
            should.not.exist(err);
            should.exist(res);
            res.should.have.status(400);
            res.body.should.have.property("msg");
            done();
          }
        )
      }
    ),
    it('Test create a movement of a account that has only madatory property', function(done){
      let dataJSON = {
         "type": "Ingreso",
         "amount": 538.16,
         "currency": "EUR",
         "date": "24/03/2018",
         "source": "Photolist",
         "subject": "Pears - Bosc",
         "account_id": ACCOUNT_TEST
      }
      chai.request(URL_MOVEMENT_SERVICES)
        .post('accounts/' + ACCOUNT_TEST + '/movements')
        .set('authorization', 'JWT ' + token)
        .set('user_id', USER_TEST)
        .send(dataJSON)
        .end(
          function(err,res){
            console.log("Testing POST " + 'accounts/' + ACCOUNT_TEST + '/movements');
            console.log(err);
            should.not.exist(err);
            should.exist(res);
            res.should.have.status(201);
            res.body.should.have.property("msg");
            done();
          }
        )
      }
    ),
    it('Test create a movement of a account in USD that has only madatory property', function(done){
      let dataJSON = {
         "type": "Ingreso",
         "amount": 538.16,
         "currency": "USD",
         "date": "24/03/2018",
         "source": "Photolist",
         "subject": "Pears - Bosc",
         "account_id": ACCOUNT_TEST
      }
      chai.request(URL_MOVEMENT_SERVICES)
        .post('accounts/' + ACCOUNT_TEST + '/movements')
        .set('authorization', 'JWT ' + token)
        .set('user_id', USER_TEST)
        .send(dataJSON)
        .end(
          function(err,res){
            console.log("Testing POST " + 'accounts/' + ACCOUNT_TEST + '/movements');
            console.log(err);
            should.not.exist(err);
            should.exist(res);
            res.should.have.status(201);
            res.body.should.have.property("msg");
            done();
          }
        )
      }
    )
  }
);

function deleteCollecion(collection){
  console.log("deleteCollecion... collection:" + collection );
  let mlabUrl = collection + "?" + mlabAPIKey;
  console.log("mlabUrl" + mlabUrl);
  let httpClient = requestJSON.createClient(mlabBaseURL);
  httpClient.put(mlabUrl, [],
    function(err, resMLab, body) {
      console.log("Response mLab:" + resMLab.statusCode);
      return (!err) ? true : false;
    }
  );
}

function insertManyCollection(collection,file){
  console.log("insertManyCollection... collection:" + collection + ", file:" + file);
  let movements = require(file);
  let mlabUrl = collection + "?" + mlabAPIKey;
  console.log("mlabUrl" + mlabUrl);
  let responseJSON = {};
  let httpClient = requestJSON.createClient(mlabBaseURL);
  httpClient.post(mlabUrl, movements,
    function(err, resMLab, body) {
      console.log("Response mLab:" + resMLab.statusCode);
      return (!err) ? true : false;
    }
  );
}
