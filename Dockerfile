# Dockerfile Easy Money Bank Bank Services

# Imagen raiz
FROM node

# Carpeta trabajo
WORKDIR /easyMoneyBankBankServices

# Añado archivos de mi aplicación
ADD . /easyMoneyBankBankServices

# Establecemos variables de entorno


# Instalo los paquetes necesarios
RUN npm install --production

# Exponemos el puerto
EXPOSE 3000

CMD ["node","server.js"]
